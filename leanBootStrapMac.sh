#!/bin/sh

# Homebrew Script for OSX
# To execute: save and `chmod +x ./brew-install-script.sh` then `./brew-install-script.sh`

echo "Installing or updating brew..."
if [[ $(command -v brew) == "" ]]; then
    echo "Installing Hombrew"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Updating Homebrew"
    brew update
fi

#Quicklook plugins
brew install --cask --no-quarantine syntax-highlight qlstephen qlmarkdown quicklook-json qlimagesize webpquicklook suspicious-package quicklookase qlvideo



#Web Tools
# brew install --cask google-chrome
brew install --cask firefox

#File Storage
# brew install --cask dropbox

#System tooling
brew install --cask alfred


#Various tools                      #ncurses disk usage browser
brew install --cask caffeine              #do not make the mac sleep
brew install --cask iterm2                #Terminal
brew install --cask spectacle             #windows manager for snapping
brew install --cask spotify
brew install --cask hazeover              #Make primary windows focus
# brew install --cask elmedia-player        #video player
brew install --cask vlc
brew cask install dashlane
# brew install --cask skype

#Nice to have
# brew install --cask fliqlo                #Screensaver

# setup oh my zsh
xcode-select —-install
brew install zsh zsh-completions
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


