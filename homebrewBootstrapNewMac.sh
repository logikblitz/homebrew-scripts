#!/bin/sh

# Homebrew Script for OSX
# To execute: save and `chmod +x ./brew-install-script.sh` then `./brew-install-script.sh`

echo "Installing or updating brew..."
if [[ $(command -v brew) == "" ]]; then
    echo "Installing Hombrew"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Updating Homebrew"
    brew update
fi


#Quicklook plugins
brew install --cask --no-quarantine syntax-highlight qlstephen qlmarkdown quicklook-json qlimagesize webpquicklook suspicious-package quicklookase qlvideo

#Programming Languages
# brew tap isen-ng/dotnet-sdk-versions
# brew install --cask dotnet-sdk7-0-100

#GO
#brew install go



#Dev Tools
brew install --cask visual-studio-code
brew install --cask azure-data-studio
brew install --cask microsoft-azure-storage-explorer
brew install --cask rider
brew install ack                        #like grep, optimized for programmers
brew install git
# brew install git-flow

#docker and kubernetes
# brew install nerdctl

#Web Tools
# brew install --cask google-chrome
brew install --cask firefox
# brew install --cask brave-browser

#File Storage
#brew install --cask google-drive
#brew install --cask dropbox

#System tooling
# brew install --cask menumeters
brew install --cask stats
brew install --cask alfred
#brew install --cask betterzip
brew install --cask insomnia
brew install --cask path-finder
# brew install --cask remote-desktop-manager-free
brew install --cask agenda
brew install mas                        # APPstore from the console. Small CLI



#Various tools
brew install ccrypt                     # strong file crypt tool
brew install geoip                      #find where an IP address comes from
brew install hh                         #shell history browser / searcher
brew install htop                       # best top replacement
brew install speedtest-cli
brew install wget
brew install ncdu                       #ncurses disk usage browser
brew install --cask caffeine              #do not make the mac sleep
brew install --cask iterm2                #Terminal
# brew install --cask kdiff3              #Diff tool
#brew install --cask spectacle             #windows manager for snapping
brew install --cask spotify
brew install --cask the-unarchiver
brew install --cask transmission
brew install --cask hazeover              #Make primary windows focus
brew install --cask elmedia-player        #video player
brew install --cask vlc
brew install --cask cyberduck           #FTP Client
brew install --cask fork                # Git IDE
brew install --cask gas-mask            #host file editor
brew install --cask handbrake           # Video Compression
brew install --cask hazeover               #https://hazeover.com/
brew install --cask postman             # REST API development




#Social tools
# brew install --cask slack
# brew install --cask microsoft-teams



#Nice to have
#brew install --cask fliqlo                #Screensaver

# setup oh my zsh
xcode-select —-install
brew install zsh zsh-completions
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"




