#!/bin/sh
brew tap | grep 'homebrew/cask' &> /dev/null
if [ $? != 0 ]; then
   echo "Installing brew cask..."
   brew tap caskroom/cask
else
    echo "brew cask already installed"
fi